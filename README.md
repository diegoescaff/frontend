

## Build

Run ` docker build -t front-angular . ` to build the project. 

```
docker build -t front-angular .
```


## Development server

Run ` docker run -it -p 4200:4200  fronted-angular ` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

