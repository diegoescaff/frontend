import { Injectable } from "@angular/core";
import { HttpResponse, HttpClient } from "@angular/common/http";

import { URL_SERVICIOS } from "../../../../config/config";

@Injectable({
  providedIn: "root"
})
export class HitsService {
  constructor(public http: HttpClient) {}

  findHits() {
    const url = URL_SERVICIOS + "/prueba";
    return this.http.get(url);
  }

  deletedHits(id: string) {
    const url = URL_SERVICIOS + "/prueba/" + id;
    return this.http.put(url, id);
  }
}
