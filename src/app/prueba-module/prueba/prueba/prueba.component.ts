import { Component, OnInit } from "@angular/core";
import { HitsService } from "./prueba.service";
import * as moment from "moment";
@Component({
  selector: "app-prueba",
  templateUrl: "./prueba.component.html",
  styleUrls: ["./prueba.component.css"]
})
export class PruebaComponent implements OnInit {
  hits: any[] = [];
  borrar: boolean;
  displayedColumns: string[] = ["title", "author", "created", "eraser"];
  dataSource = [];
  constructor(public hitsService: HitsService) {}

  ngOnInit() {
    this.findAllhists();
  }
  prueba(e, i) {
    if (e.type == "mouseenter") {
      this.hits[i]["ver"] = true;
    } else {
      this.hits[i]["ver"] = false;
    }
  }

  findAllhists() {
    const DIA = Date.now();
    this.hitsService.findHits().subscribe((res: Array<any>) => {
      this.hits = res["hits"].filter(hit => hit.deleted == null);
      console.log(this.hits);
      this.hits.forEach((element, index) => {
        if (moment(DIA).format("L") == moment(element.created_at).format("L")) {
          element.created_at = element.created_at;
        }

        if (
          moment(element.created_at)
            .calendar()
            .includes("Today")
        ) {
          element.created_at = moment(element.created_at).format("LT");
        }

        if (
          moment(element.created_at)
            .calendar()
            .includes("Yesterday")
        ) {
          element.created_at = "Yesterday";
        }

        if (
          moment(element.created_at)
            .calendar()
            .includes("Last")
        ) {
          element.created_at = moment(element.created_at).format("ll");
        }
        element.ver = false;
      });

      this.dataSource = this.hits;
    });
  }

  deletedhit(idHit: string) {
    console.log(idHit);

    this.hitsService.deletedHits(idHit).subscribe((res: Array<any>) => {
      console.log(res);
      this.findAllhists();
    });
  }

  openURL(url: string) {
    window.open(url, "_blank");
  }
}
